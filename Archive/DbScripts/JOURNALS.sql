--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table JOURNALS
--------------------------------------------------------

  CREATE TABLE "LMS"."JOURNALS" 
   (	"JOURNAL_ID" NUMBER, 
	"VOLUME" NUMBER, 
	"ISSUE" VARCHAR2(20 BYTE), 
	"JOURNAL_EDITOR" VARCHAR2(20 BYTE), 
	"PUBLISH_DATE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.JOURNALS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index JOURNALS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."JOURNALS_PK" ON "LMS"."JOURNALS" ("JOURNAL_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table JOURNALS
--------------------------------------------------------

  ALTER TABLE "LMS"."JOURNALS" MODIFY ("JOURNAL_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."JOURNALS" ADD CONSTRAINT "JOURNALS_PK" PRIMARY KEY ("JOURNAL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table JOURNALS
--------------------------------------------------------

  ALTER TABLE "LMS"."JOURNALS" ADD CONSTRAINT "JOURNALS_FK1" FOREIGN KEY ("JOURNAL_ID")
	  REFERENCES "LMS"."LIBRARY_ITEMS" ("LIB_ITEM_ID") ENABLE;
