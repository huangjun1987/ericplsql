--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ISSUE_DETAILS
--------------------------------------------------------

  CREATE TABLE "LMS"."ISSUE_DETAILS" 
   (	"ISSUE_DETAIL_ID" NUMBER, 
	"ITEM_COPY_ID" NUMBER, 
	"MEMBER_ID" NUMBER, 
	"ISSUE_DATE" DATE, 
	"DUE_DATE" DATE, 
	"RETURN_DATE" DATE, 
	"COMMENTS" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.ISSUE_DETAILS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index ISSUE_DETAILS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."ISSUE_DETAILS_PK" ON "LMS"."ISSUE_DETAILS" ("ISSUE_DETAIL_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table ISSUE_DETAILS
--------------------------------------------------------

  ALTER TABLE "LMS"."ISSUE_DETAILS" MODIFY ("ISSUE_DETAIL_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."ISSUE_DETAILS" ADD CONSTRAINT "ISSUE_DETAILS_PK" PRIMARY KEY ("ISSUE_DETAIL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ISSUE_DETAILS
--------------------------------------------------------

  ALTER TABLE "LMS"."ISSUE_DETAILS" ADD CONSTRAINT "ISSUE_DETAILS_FK1" FOREIGN KEY ("ITEM_COPY_ID")
	  REFERENCES "LMS"."LIBRARY_ITEM_COPIES" ("ITEM_COPY_ID") ENABLE;
  ALTER TABLE "LMS"."ISSUE_DETAILS" ADD CONSTRAINT "ISSUE_DETAILS_FK2" FOREIGN KEY ("MEMBER_ID")
	  REFERENCES "LMS"."MEMBERS" ("MEMBER_ID") ENABLE;
insert into ISSUE_DETAILS
values
(1,
1,
1,
TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2003/06/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2003/05/13 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
'Damaged'
)

insert into ISSUE_DETAILS
values
(2,
1,
1,
TO_DATE('2013/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2013/06/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2013/05/13 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
'Damaged'
)

update ISSUE_DETAILS
set ISSUE_DATE = TO_DATE('2020/01/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss')
where ISSUE_DETAIL_ID = 1

insert into ISSUE_DETAILS
values
(3,
1,
1,
TO_DATE('2013/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2013/06/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2013/05/13 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
'Damaged'
)

insert into ISSUE_DETAILS
values
(4,
1,
1,
TO_DATE('2020/03/01 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2013/06/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2013/05/13 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
'Damaged'
)
insert into ISSUE_DETAILS
values
(5,
1,
1,
TO_DATE('2020/02/28 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2013/06/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2013/05/13 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
'Damaged'
)