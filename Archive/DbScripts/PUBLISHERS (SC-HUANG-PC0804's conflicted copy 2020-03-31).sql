--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PUBLISHERS
--------------------------------------------------------

  CREATE TABLE "LMS"."PUBLISHERS" 
   (	"PUB_ID" NUMBER, 
	"PUB_NAME" VARCHAR2(20 BYTE), 
	"CITY" VARCHAR2(20 BYTE), 
	"STATE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.PUBLISHERS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index PUBLISHERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."PUBLISHERS_PK" ON "LMS"."PUBLISHERS" ("PUB_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table PUBLISHERS
--------------------------------------------------------

  ALTER TABLE "LMS"."PUBLISHERS" MODIFY ("PUB_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."PUBLISHERS" ADD CONSTRAINT "PUBLISHERS_PK" PRIMARY KEY ("PUB_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
  insert into PUBLISHERS
  values
  (1,
  'PUBLISHERS1',
  'Chicago',
  'IL'
  )
  insert into PUBLISHERS
  values
  (2,
  'PUBLISHERS1',
  'Seattle',
  'WA'
  )
  
   insert into PUBLISHERS
  values
  (3,
  'PUBLISHERS2',
  'Spokane',
  'WA'
  )