--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table BOOKS
--------------------------------------------------------

  CREATE TABLE "LMS"."BOOKS" 
   (	"BOOK_ID" NUMBER, 
	"GENRE" VARCHAR2(20 BYTE), 
	"EDITION" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.BOOKS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index BOOKS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."BOOKS_PK" ON "LMS"."BOOKS" ("BOOK_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table BOOKS
--------------------------------------------------------

  ALTER TABLE "LMS"."BOOKS" MODIFY ("BOOK_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."BOOKS" ADD CONSTRAINT "BOOKS_PK" PRIMARY KEY ("BOOK_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BOOKS
--------------------------------------------------------

  ALTER TABLE "LMS"."BOOKS" ADD CONSTRAINT "BOOKS_FK1" FOREIGN KEY ("BOOK_ID")
	  REFERENCES "LMS"."LIBRARY_ITEMS" ("LIB_ITEM_ID") ENABLE;

insert into BOOKS
Values
(
1,
'Fantasy Books',
'1'
)
insert into BOOKS
Values
(
2,
'Mystery Books',
'1'
)

update Books 
set genre='Fantasy Books'
where book_id=1
