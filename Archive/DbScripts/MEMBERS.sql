--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table MEMBERS
--------------------------------------------------------

  CREATE TABLE "LMS"."MEMBERS" 
   (	"MEMBER_ID" NUMBER, 
	"FIRST_NAME" VARCHAR2(20 BYTE), 
	"LAST_NAME" VARCHAR2(20 BYTE), 
	"MEMBER_SINCE" DATE, 
	"EXPIRY_DATE" DATE, 
	"NO_OF_ITEMS_ISSUED" NUMBER(*,0), 
	"ADDRESS_1" VARCHAR2(20 BYTE), 
	"ADDRESS_2" VARCHAR2(20 BYTE), 
	"CITY" VARCHAR2(20 BYTE), 
	"STATE" VARCHAR2(20 BYTE), 
	"ZIP_CODE" VARCHAR2(20 BYTE), 
	"PHONE_NUM" VARCHAR2(20 BYTE), 
	"BALANCE" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.MEMBERS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index MEMBERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."MEMBERS_PK" ON "LMS"."MEMBERS" ("MEMBER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table MEMBERS
--------------------------------------------------------

  ALTER TABLE "LMS"."MEMBERS" MODIFY ("MEMBER_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."MEMBERS" ADD CONSTRAINT "MEMBERS_PK" PRIMARY KEY ("MEMBER_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
select * from MEMBERS

update members
set member_since = TO_DATE('2019/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss')
where member_ID = 1

insert into members
values(
2,
'Jane',
'Li',
TO_DATE('2018/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2019/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
2,
'MEMBERS2 ADDRESS1',
'MEMBERS2 ADDRESS2',
'Seattle',
'WA',
'12345',
'2222222222',
22
)