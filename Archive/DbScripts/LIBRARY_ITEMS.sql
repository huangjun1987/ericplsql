--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table LIBRARY_ITEMS
--------------------------------------------------------

  CREATE TABLE "LMS"."LIBRARY_ITEMS" 
   (	"LIB_ITEM_ID" NUMBER, 
	"TITLE" VARCHAR2(20 BYTE), 
	"ITEM_TYPE" VARCHAR2(20 BYTE), 
	"NO_OF_PAGES" NUMBER(*,0), 
	"STANDARD_PRICE" NUMBER, 
	"PUB_ID" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.LIBRARY_ITEMS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index LIBRARY_ITEMS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."LIBRARY_ITEMS_PK" ON "LMS"."LIBRARY_ITEMS" ("LIB_ITEM_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table LIBRARY_ITEMS
--------------------------------------------------------

  ALTER TABLE "LMS"."LIBRARY_ITEMS" MODIFY ("LIB_ITEM_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."LIBRARY_ITEMS" ADD CONSTRAINT "LIBRARY_ITEMS_PK" PRIMARY KEY ("LIB_ITEM_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LIBRARY_ITEMS
--------------------------------------------------------

  ALTER TABLE "LMS"."LIBRARY_ITEMS" ADD CONSTRAINT "LIBRARY_ITEMS_FK1" FOREIGN KEY ("PUB_ID")
	  REFERENCES "LMS"."PUBLISHERS" ("PUB_ID") ENABLE;
insert into LIBRARY_ITEMS
values(
1,
'LIBRARY_ITEMS1',
'1',
100,
100,
1
)
insert into LIBRARY_ITEMS
values(
2,
'LIBRARY_ITEMS2',
'1',
100,
100,
1
)