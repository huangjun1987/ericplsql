--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table MAGAZINES
--------------------------------------------------------

  CREATE TABLE "LMS"."MAGAZINES" 
   (	"MAGAZINE_ID" NUMBER, 
	"EDITION" VARCHAR2(20 BYTE), 
	"TYPE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.MAGAZINES
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index MAGAZINES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."MAGAZINES_PK" ON "LMS"."MAGAZINES" ("MAGAZINE_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table MAGAZINES
--------------------------------------------------------

  ALTER TABLE "LMS"."MAGAZINES" MODIFY ("MAGAZINE_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."MAGAZINES" ADD CONSTRAINT "MAGAZINES_PK" PRIMARY KEY ("MAGAZINE_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MAGAZINES
--------------------------------------------------------

  ALTER TABLE "LMS"."MAGAZINES" ADD CONSTRAINT "MAGAZINES_FK1" FOREIGN KEY ("MAGAZINE_ID")
	  REFERENCES "LMS"."LIBRARY_ITEMS" ("LIB_ITEM_ID") ENABLE;
