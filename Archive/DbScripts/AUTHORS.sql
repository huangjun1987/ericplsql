--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table AUTHORS
--------------------------------------------------------

  CREATE TABLE "LMS"."AUTHORS" 
   (	"AU_ID" NUMBER, 
	"AU_LNAME" VARCHAR2(20 BYTE), 
	"AU_FNAME" VARCHAR2(20 BYTE), 
	"PHONE" VARCHAR2(20 BYTE), 
	"ADDRESS" VARCHAR2(20 BYTE), 
	"CITY" VARCHAR2(20 BYTE), 
	"STATE" VARCHAR2(20 BYTE), 
	"COUNTRY" VARCHAR2(20 BYTE), 
	"POSTALCODE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.AUTHORS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index AUTHORS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."AUTHORS_PK" ON "LMS"."AUTHORS" ("AU_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table AUTHORS
--------------------------------------------------------

  ALTER TABLE "LMS"."AUTHORS" MODIFY ("AU_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."AUTHORS" ADD CONSTRAINT "AUTHORS_PK" PRIMARY KEY ("AU_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;

insert into AUTHORS
  VALUES
  (1,
  'Doe',
  'John',
  '1111111111',
  'AUTHORS ADDRESS1',
  'Chicago',
  'IL',
  'US',
  '60616'
  )