--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table LIBRARY_ITEM_COPIES
--------------------------------------------------------

  CREATE TABLE "LMS"."LIBRARY_ITEM_COPIES" 
   (	"ITEM_COPY_ID" NUMBER, 
	"DATE_OF_PURCHASE" DATE, 
	"PURCHASE_PRICE" NUMBER, 
	"IS_BOUND" VARCHAR2(20 BYTE), 
	"IS_ISSUED" VARCHAR2(20 BYTE), 
	"LIB_ITEM_ID" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.LIBRARY_ITEM_COPIES
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index LIBRARY_ITEM_COPIES_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."LIBRARY_ITEM_COPIES_PK" ON "LMS"."LIBRARY_ITEM_COPIES" ("ITEM_COPY_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table LIBRARY_ITEM_COPIES
--------------------------------------------------------

  ALTER TABLE "LMS"."LIBRARY_ITEM_COPIES" MODIFY ("ITEM_COPY_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."LIBRARY_ITEM_COPIES" ADD CONSTRAINT "LIBRARY_ITEM_COPIES_PK" PRIMARY KEY ("ITEM_COPY_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table LIBRARY_ITEM_COPIES
--------------------------------------------------------

  ALTER TABLE "LMS"."LIBRARY_ITEM_COPIES" ADD CONSTRAINT "LIBRARY_ITEM_COPIES_FK1" FOREIGN KEY ("LIB_ITEM_ID")
	  REFERENCES "LMS"."LIBRARY_ITEMS" ("LIB_ITEM_ID") ENABLE;
      
      select * from LIBRARY_ITEM_COPIES
insert into LIBRARY_ITEM_COPIES
values 
(1,
TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
100,
'Y',
'Y',
1)

insert into LIBRARY_ITEM_COPIES
values 
(2,
TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
100,
'Y',
'Y',
1)