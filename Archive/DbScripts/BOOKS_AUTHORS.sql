--------------------------------------------------------
--  File created - Monday-March-23-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table BOOKS_AUTHORS
--------------------------------------------------------

  CREATE TABLE "LMS"."BOOKS_AUTHORS" 
   (	"BOOK_ID" NUMBER, 
	"AU_ID" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into LMS.BOOKS_AUTHORS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index BOOKS_AUTHORS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "LMS"."BOOKS_AUTHORS_PK" ON "LMS"."BOOKS_AUTHORS" ("BOOK_ID", "AU_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table BOOKS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "LMS"."BOOKS_AUTHORS" MODIFY ("BOOK_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."BOOKS_AUTHORS" MODIFY ("AU_ID" NOT NULL ENABLE);
  ALTER TABLE "LMS"."BOOKS_AUTHORS" ADD CONSTRAINT "BOOKS_AUTHORS_PK" PRIMARY KEY ("BOOK_ID", "AU_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BOOKS_AUTHORS
--------------------------------------------------------

  ALTER TABLE "LMS"."BOOKS_AUTHORS" ADD CONSTRAINT "BOOKS_AUTHORS_FK1" FOREIGN KEY ("AU_ID")
	  REFERENCES "LMS"."AUTHORS" ("AU_ID") ENABLE;
  ALTER TABLE "LMS"."BOOKS_AUTHORS" ADD CONSTRAINT "BOOKS_AUTHORS_FK2" FOREIGN KEY ("BOOK_ID")
	  REFERENCES "LMS"."BOOKS" ("BOOK_ID") ENABLE;
insert into BOOKS_AUTHORS
values
(1,
1)
insert into BOOKS_AUTHORS
values
(2,
1)