/*Question-1
a) For each customer, display their customer code (heading: Customer ID), Customer first name
(heading: First Name), customer initial (heading: Middle Initial), and last name (heading: Last
Name), customer area code (heading: Area code) and phone number (heading: contact
number).*/
SELECT cus_code AS "Customer ID",cus_fname AS
"First Name",cus_initial AS "Middle Intial",cus_lname AS "Last Name",
cus_areacode AS "Area Code" ,cus_phone AS "Contact Number"
FROM AVIA.Customer
;
/*
b) Additionally (i.e., in addition to requirement �a.� above): display the customer balance (heading:
Customer Balance). Restrict yourself to displaying only those customers who have a customer
balance strictly more than 500. Format this column using: $999,999.99
*/
SELECT cus_code AS "Customer ID",cus_fname AS
"First Name",cus_initial AS "Middle Intials",cus_lname AS "Last Name",
cus_areacode AS "Area Code" ,cus_phone AS "Contact Number", 
to_char(cus_balance, '$999,999.00') as Balance --change integer format to dollar value with business friendly name
FROM AVIA.Customer
WHERE cus_balance > 500 --removes any cusomter with a balance less that $500
;
/*
c) Customers who have a NULL middle initial should not be displayed.
*/
SELECT cus_code AS "Customer ID",cus_fname AS
"First Name",cus_initial AS "Middle Intials",cus_lname AS "Last Name",
cus_areacode AS "Area Code" ,cus_phone AS "Contact Number", --list business friendly columns
to_char(cus_balance, '$999,999.00') as Balance --change integer format to dollar value with business friendly name
FROM AVIA.Customer
WHERE cus_balance > 500 --removes any cusomter with a balance less that $500
AND cus_initial IS NOT NULL -- no nulls can exist in middle intital
;
/*
d) Restrict yourself to customers whose last name begins with the letter: O
*/
SELECT cus_code AS "Customer ID",cus_fname AS
"First Name",cus_initial AS "Middle Intials",cus_lname AS "Last Name",
cus_areacode AS "Area Code" ,cus_phone AS "Contact Number", --list business friendly columns
to_char(cus_balance, '$999,999.00') as Balance --change integer format to dollar value with business friendly name
FROM AVIA.Customer
WHERE cus_balance > 500 --removes any cusomter with a balance less that $500
AND cus_initial IS NOT NULL -- no nulls can exist in middle intital
And cus_lname like 'O%'
;
/*
e) Sort the results by their last name (Alphabetically), and within that by first name.*/
SELECT cus_code AS "Customer ID",cus_fname AS
"First Name",cus_initial AS "Middle Intials",cus_lname AS "Last Name",
cus_areacode AS "Area Code" ,cus_phone AS "Contact Number", --list business friendly columns
to_char(cus_balance, '$999,999.00') as "Balance" --change integer format to dollar value with business friendly name
FROM AVIA.Customer
WHERE cus_balance > 500 --removes any cusomter with a balance less that $500
AND cus_initial IS NOT NULL -- no nulls can exist in middle intital
And cus_lname like 'O%'
ORDER BY cus_lname, cus_fname -- sort by last name, then first name
;

/*Question-2
a) For each charter trip, display the char_trip (heading: Trip ID) along with its date (heading: Trip
date).
*/
SELECT c.char_trip as "Trip ID", c.CHAR_DATE as "Trip
date"
FROM AVIA.Charter c
/*
b) Also display the aircraft number (heading: Aircraft No) used in the trip, the manufacturer
(heading: Make), and the model name (heading: Model).
*/
SELECT c.char_trip as "Trip ID", c.CHAR_DATE as "Trip date",
ac.AC_NUMBER as "Aircraft No", m.mod_manufacturer as "Make",
m.MOD_NAME as "Model"
FROM AVIA.Charter c
join AVIA.aircraft ac on c.AC_NUMBER = ac.AC_NUMBER
JOIN AVIA.model m on m.mod_code = ac.mod_code

/*
c) Calculate the basic cost for each trip (heading: Trip Cost). Format the output using a
$9,999,999.99 template. The basic cost per trip is calculated as the charter trip distance times
the price per mile (for that aircraft model). E.g., if the trip distance is 100 miles and the cost per
mile for that model is 5, then the cost is: 500.
*/
SELECT c.char_trip as "Trip ID", c.CHAR_DATE as "Trip date",
ac.AC_NUMBER as "Aircraft No", m.mod_manufacturer as "Make",
m.MOD_NAME as "Model",
to_char(c.CHAR_DISTANCE*m.MOD_CHG_MILE, '$999,999.99') as "Trip Cost"
FROM AVIA.Charter c
join AVIA.aircraft ac on c.AC_NUMBER = ac.AC_NUMBER
JOIN AVIA.model m on m.mod_code = ac.mod_code
/*
d) Display only those trips with a basic cost of $2000 or higher.
*/
SELECT c.char_trip as "Trip ID", c.CHAR_DATE as "Trip date",
ac.AC_NUMBER as "Aircraft No", m.mod_manufacturer as "Make",
m.MOD_NAME as "Model",
to_char(c.CHAR_DISTANCE*m.MOD_CHG_MILE, '$999,999.99') as "Trip Cost"
FROM AVIA.Charter c
join AVIA.aircraft ac on c.AC_NUMBER = ac.AC_NUMBER
JOIN AVIA.model m on m.mod_code = ac.mod_code
where c.CHAR_DISTANCE*m.MOD_CHG_MILE >= 2000
/*
e) Restrict your results to trips made on or before February 15, 2018.
*/
SELECT c.char_trip as "Trip ID", c.CHAR_DATE as "Trip date",
ac.AC_NUMBER as "Aircraft No", m.mod_manufacturer as "Make",
m.MOD_NAME as "Model",
to_char(c.CHAR_DISTANCE*m.MOD_CHG_MILE, '$999,999.99') as "Trip Cost"
FROM AVIA.Charter c
join AVIA.aircraft ac on c.AC_NUMBER = ac.AC_NUMBER
JOIN AVIA.model m on m.mod_code = ac.mod_code
where c.CHAR_DISTANCE*m.MOD_CHG_MILE >= 2000
and c.CHAR_DATE <= TO_DATE('2018/02/15 21:02:44', 'yyyy/mm/dd hh24:mi:ss')
/*
f) Sort the data by Trip Cost (highest on top).*/
SELECT c.char_trip as "Trip ID", c.CHAR_DATE as "Trip date",
ac.AC_NUMBER as "Aircraft No", m.mod_manufacturer as "Make",
m.MOD_NAME as "Model",
to_char(c.CHAR_DISTANCE*m.MOD_CHG_MILE, '$999,999.99') as "Trip Cost"
FROM AVIA.Charter c
join AVIA.aircraft ac on c.AC_NUMBER = ac.AC_NUMBER
JOIN AVIA.model m on m.mod_code = ac.mod_code
where c.CHAR_DISTANCE*m.MOD_CHG_MILE >= 2000
and c.CHAR_DATE <= TO_DATE('2018/02/15 21:02:44', 'yyyy/mm/dd hh24:mi:ss')
ORDER BY c.CHAR_DISTANCE*m.MOD_CHG_MILE
/*
Question-3
a) For each aircraft that has flown a charter trip, display the aircraft number (heading: Aircraft No.)
*/
SELECT a.ac_number as "Aircraft Number"
FROM AVIA.aircraft a
JOIN AVIA.charter c on a.ac_number = c.ac_number
/*
b) Additionally, display the following trip related statistics for the aircraft:
� How many charter trips used that aircraft (heading: Num Trips)
� How many different trip destinations the aircraft has gone to (heading: Num Destinations)
� Total distance flown (heading: Distance Flown, format: 999,999.99)
� Average charter flight distance (heading: Average Flight Distance, format: 99,999.99)
� Total number of hours flown (heading: Hours flown, format: 9,999.99)
� Total number of operational hours, i.e., combining hours flown and the waiting time (heading: Billable Hours, format: 9,999.99).
� Average fuel gallons consumed per charter rounded to the nearest gallon (heading: Avg Fuel/gal, format 9,999). Note: you can search the web for how to round up.
Hint: use GROUP BY
*/
SELECT a.ac_number as "Aircraft Number", 
count(c.CHAR_TRIP) as "Num Trips",
count(distinct(c.CHAR_DESTINATION)) as "Num Destinations",
to_char(sum(c.CHAR_DISTANCE), '$9,999.99') as "Distance Flown",
to_char(sum(c.CHAR_HOURS_FLOWN + c.CHAR_HOURS_WAIT), '9,999.99') as "Billable Hours",
to_char(ROUND(avg(CHAR_FUEL_GALLONS),0), '9,999') as "Avg Fuel/gal"
FROM AVIA.aircraft a
JOIN AVIA.charter c on a.ac_number = c.ac_number
group by a.ac_number

/*
Question-4
a) Display each aircraft�s number (heading: Aircraft number), manufacturer (heading: Manufacturer) and model name (heading: Model).
*/
SELECT a.ac_number as "Aircraft Number", m.mod_manufacturer as "Make", m.MOD_NAME as "Model"
FROM AVIA.aircraft a
JOIN AVIA.model m on a.MOD_CODE = m.MOD_CODE

/*
b) Also show the destinations the aircraft has gone to (heading: Destination) and the average wait time (heading: Avg Wait Time, format 999.99) for each of these destinations.
*/
SELECT a.ac_number as "Aircraft Number", m.mod_manufacturer as "Make", m.MOD_NAME as "Model",
c.CHAR_DESTINATION,
to_char(avg(c.CHAR_HOURS_WAIT), '999.99') as "Avg Wait Time"
FROM AVIA.aircraft a
JOIN AVIA.model m on a.MOD_CODE = m.MOD_CODE
JOIN AVIA.charter c on a.ac_number = c.ac_number
Group by a.ac_number, c.CHAR_DESTINATION , m.mod_manufacturer, m.MOD_NAME

/*
c) Only show destinations where the aircraft had an average wait time > 1.
*/
SELECT a.ac_number as "Aircraft Number", m.mod_manufacturer as "Make", m.MOD_NAME as "Model",
c.CHAR_DESTINATION,
to_char(avg(c.CHAR_HOURS_WAIT), '999.99') as "Avg Wait Time"
FROM AVIA.aircraft a
JOIN AVIA.model m on a.MOD_CODE = m.MOD_CODE
JOIN AVIA.charter c on a.ac_number = c.ac_number
Group by a.ac_number, c.CHAR_DESTINATION , m.mod_manufacturer, m.MOD_NAME
having c.CHAR_DESTINATION in (select c2.CHAR_DESTINATION from AVIA.charter c2 group by c2.CHAR_DESTINATION having avg(c2.CHAR_HOURS_WAIT)>1)
/*
d) Find out how many different customers have booked charters on the aircraft for that destination (heading: Customers).
*/
SELECT a.ac_number as "Aircraft Number", m.mod_manufacturer as "Make", m.MOD_NAME as "Model",
c.CHAR_DESTINATION,
to_char(avg(c.CHAR_HOURS_WAIT), '999.99') as "Avg Wait Time", count(distinct(c.CUS_CODE)) as "Customers"
FROM AVIA.aircraft a
JOIN AVIA.model m on a.MOD_CODE = m.MOD_CODE
JOIN AVIA.charter c on a.ac_number = c.ac_number
Group by a.ac_number, c.CHAR_DESTINATION , m.mod_manufacturer, m.MOD_NAME
having c.CHAR_DESTINATION in (select c2.CHAR_DESTINATION from AVIA.charter c2 group by c2.CHAR_DESTINATION having avg(c2.CHAR_HOURS_WAIT)>1)

/*
e) Sort the results by the manufacturer and within that by model name.
Hint: you will need to use the HAVING clause
*/
SELECT a.ac_number as "Aircraft Number", m.mod_manufacturer as "Make", m.MOD_NAME as "Model",
c.CHAR_DESTINATION,
to_char(avg(c.CHAR_HOURS_WAIT), '999.99') as "Avg Wait Time", count(distinct(c.CUS_CODE)) as "Customers"
FROM AVIA.aircraft a
JOIN AVIA.model m on a.MOD_CODE = m.MOD_CODE
JOIN AVIA.charter c on a.ac_number = c.ac_number
Group by a.ac_number, c.CHAR_DESTINATION , m.mod_manufacturer, m.MOD_NAME
having c.CHAR_DESTINATION in (select c2.CHAR_DESTINATION from AVIA.charter c2 group by c2.CHAR_DESTINATION having avg(c2.CHAR_HOURS_WAIT)>1)
ORDER BY m.mod_manufacturer, m.MOD_NAME 

/*
Question-5
--a) For each employee display the employee number (heading: Employee Num) and last name (heading: Last Name).
*/
SELECT e.emp_num as "Employee Num", e.emp_lname as "Last Name"
FROM AVIA.employee e
;
/*
--b) Next, show the different crew jobs (roles, e.g., Pilot / Engineer / Co-Pilot) the employee has served.
--Some employee may serve in only one crew job, while others may have more than one.
*/
SELECT e.emp_num as "Employee Num", e.emp_lname as "Last Name", c.crew_job
FROM AVIA.employee e JOIN AVIA.crew c ON c.emp_num = e.emp_num
;
/*
c)Display how many charter trips each employee was associated with (heading: Num Trips)
for each of their crew jobs.
You should have a separate row in your output for each crew job the employee has served in
(i.e., it�s ok that the same employee shows up more than once if they are serving different crew jobs)
.E.g., if Wilbur Wildcat flew 3trips as a Copilot and2trips as a Pilot, you expect to see two output rows for Wilbur Wildcat
*/
SELECT e.emp_num as "Employee Num", e.emp_lname as "Last Name", c.crew_job, count(c.CHAR_TRIP)
FROM AVIA.employee e JOIN AVIA.crew c ON c.emp_num = e.emp_num
group by e.emp_num,e.emp_lname, c.crew_job
/*
d)Additionally, display the following statistics for the employee�crew job pair:
�Number of different aircraft acrosscharters(heading: Num Aircraft)
�Maximum total time on the airframe (TTAF) across charter aircraft (heading: MaxTTAF, format: 99,999.99)
�Minimum total time on the airframe (TTAF) across charter aircraft (heading: Min TTAF, format: 99,999.99)e)
For the calculated statistics: only include charters with a distance that�s (strictly) above the average charter distance (calculated across all charters). Note: if you can�t figure this part out, move on to subsequent queries and come back later.
*/
SELECT e.emp_num as "Employee Num", e.emp_lname as "Last Name", c.crew_job, count(c.CHAR_TRIP),
count(distinct(ch.AC_NUMBER)) as "Num Aircraft",
to_char(max(a.AC_TTAF), '$999,999.99') as "MaxTTAF"
FROM AVIA.employee e JOIN AVIA.crew c ON c.emp_num = e.emp_num
join 
( select * from 
AVIA.Charter c1
where c1.CHAR_DISTANCE > (select avg(c2.CHAR_DISTANCE) from AVIA.Charter c2)
)
ch on c.char_trip = ch.char_trip
join AVIA.AIRCRAFT a on a.AC_NUMBER = ch.AC_NUMBER
group by e.emp_num,e.emp_lname, c.crew_job
/*
f)Sort the results by the employee numb*/
SELECT e.emp_num as "Employee Num", e.emp_lname as "Last Name", c.crew_job, count(c.CHAR_TRIP),
count(distinct(ch.AC_NUMBER)) as "Num Aircraft",
to_char(max(a.AC_TTAF), '$999,999.99') as "MaxTTAF"
FROM AVIA.employee e JOIN AVIA.crew c ON c.emp_num = e.emp_num
join 
( select * from 
AVIA.Charter c1
where c1.CHAR_DISTANCE > (select avg(c2.CHAR_DISTANCE) from AVIA.Charter c2)
)
ch on c.char_trip = ch.char_trip
join AVIA.AIRCRAFT a on a.AC_NUMBER = ch.AC_NUMBER
group by e.emp_num,e.emp_lname, c.crew_job
order by e.emp_num
;

/*
Question-6
a) Display for customers their customer code (heading: Customer Id) Also display a full name
column combing the first and last names with a space in between, e.g., John Doe(heading: Customer Name)
*/
SELECT c.cus_code AS "Customer ID", c.cus_fname||' '||c.cus_lname as "Customer Name"
FROM AVIA.customer c 
;

/*
b) Also show the number of trips they have chartered (heading: Num Trips),
the median trip waiting times (heading: Median Wait), the number of models of
aircraft they have flown (heading: Models Flown) and the total hours flown on charters (heading: Hours Flown)
*/

SELECT
  cus_code  AS "Customer ID",
  cus_fname||' '||cus_lname as "Customer Name",
  count(char_trip) as "Num Trips",
  count(distinct(AC_NUMBER)) as "Models Flown",
sum(CHAR_HOURS_FLOWN) as "Hours Flown",
  AVG(CHAR_HOURS_WAIT)
FROM
(
   SELECT
      c.cus_code,
      c.cus_fname,
      c.cus_lname,
      h.AC_NUMBER,
      h.CHAR_HOURS_WAIT,
      h.char_trip,
      h.CHAR_HOURS_FLOWN,
      ROW_NUMBER() OVER (
         PARTITION BY c.cus_code
         ORDER BY h.CHAR_HOURS_WAIT ASC, h.AC_NUMBER ASC) AS RowAsc,
      ROW_NUMBER() OVER (
         PARTITION BY c.cus_code
         ORDER BY h.CHAR_HOURS_WAIT DESC, h.AC_NUMBER DESC) AS RowDesc
   FROM AVIA.customer c JOIN AVIA.charter h on c.cus_code = h.cus_code
) x
WHERE
   RowAsc IN (RowDesc, RowDesc - 1, RowDesc + 1)  
   group by cus_code, cus_fname||' '||cus_lname

/*
c) Restrict your results to customers whose Hours flown (i.e., total)
is at least as much as the average of the (total) Hours flown across customers.
*/
SELECT
  cus_code  AS "Customer ID",
  cus_fname||' '||cus_lname as "Customer Name",
  count(char_trip) as "Num Trips",
  count(distinct(AC_NUMBER)) as "Models Flown",
sum(CHAR_HOURS_FLOWN) as "Hours Flown",
  AVG(CHAR_HOURS_WAIT)
FROM
(
   SELECT
      c.cus_code,
      c.cus_fname,
      c.cus_lname,
      h.AC_NUMBER,
      h.CHAR_HOURS_WAIT,
      h.char_trip,
      h.CHAR_HOURS_FLOWN,
      ROW_NUMBER() OVER (
         PARTITION BY c.cus_code
         ORDER BY h.CHAR_HOURS_WAIT ASC, h.AC_NUMBER ASC) AS RowAsc,
      ROW_NUMBER() OVER (
         PARTITION BY c.cus_code
         ORDER BY h.CHAR_HOURS_WAIT DESC, h.AC_NUMBER DESC) AS RowDesc
   FROM AVIA.customer c JOIN AVIA.charter h on c.cus_code = h.cus_code
) x
WHERE
   RowAsc IN (RowDesc, RowDesc - 1, RowDesc + 1)  
   group by cus_code, cus_fname||' '||cus_lname
having sum(CHAR_HOURS_FLOWN) >= ((select sum(CHAR_HOURS_FLOWN) from CHARTER)/ (select count(*) from CUSTOMER))
;
/*
d) Sort your results by the hours flown (lowest first)
*/
SELECT
  cus_code  AS "Customer ID",
  cus_fname||' '||cus_lname as "Customer Name",
  count(char_trip) as "Num Trips",
  count(distinct(AC_NUMBER)) as "Models Flown",
sum(CHAR_HOURS_FLOWN) as "Hours Flown",
  AVG(CHAR_HOURS_WAIT)
FROM
(
   SELECT
      c.cus_code,
      c.cus_fname,
      c.cus_lname,
      h.AC_NUMBER,
      h.CHAR_HOURS_WAIT,
      h.char_trip,
      h.CHAR_HOURS_FLOWN,
      ROW_NUMBER() OVER (
         PARTITION BY c.cus_code
         ORDER BY h.CHAR_HOURS_WAIT ASC, h.AC_NUMBER ASC) AS RowAsc,
      ROW_NUMBER() OVER (
         PARTITION BY c.cus_code
         ORDER BY h.CHAR_HOURS_WAIT DESC, h.AC_NUMBER DESC) AS RowDesc
   FROM AVIA.customer c JOIN AVIA.charter h on c.cus_code = h.cus_code
) x
WHERE
   RowAsc IN (RowDesc, RowDesc - 1, RowDesc + 1)  
   group by cus_code, cus_fname||' '||cus_lname
having sum(CHAR_HOURS_FLOWN) >= ((select sum(CHAR_HOURS_FLOWN) from CHARTER)/ (select count(*) from CUSTOMER))
order by sum(CHAR_HOURS_FLOWN)
;
