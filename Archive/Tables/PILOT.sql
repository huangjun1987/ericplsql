--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PILOT
--------------------------------------------------------

  CREATE TABLE "AVIA"."PILOT" 
   (	"EMP_NUM" NUMBER, 
	"PIL_LICENSE" VARCHAR2(20 BYTE), 
	"PIL_MED_TYPE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AVIA.PILOT
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index PILOT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."PILOT_PK" ON "AVIA"."PILOT" ("EMP_NUM") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table PILOT
--------------------------------------------------------

  ALTER TABLE "AVIA"."PILOT" MODIFY ("EMP_NUM" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."PILOT" ADD CONSTRAINT "PILOT_PK" PRIMARY KEY ("EMP_NUM")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PILOT
--------------------------------------------------------

  ALTER TABLE "AVIA"."PILOT" ADD CONSTRAINT "PILOT_FK1" FOREIGN KEY ("EMP_NUM")
	  REFERENCES "AVIA"."EMPLOYEE" ("EMP_NUM") ENABLE;
