--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CUSTOMER
--------------------------------------------------------

  CREATE TABLE "AVIA"."CUSTOMER" 
   (	"CUS_CODE" NUMBER(*,0), 
	"CUS_LNAME" VARCHAR2(20 BYTE), 
	"CUS_FNAME" VARCHAR2(20 BYTE), 
	"CUS_INITIAL" VARCHAR2(20 BYTE), 
	"CUS_AREACODE" VARCHAR2(20 BYTE), 
	"CUS_PHONE" VARCHAR2(20 BYTE), 
	"CUS_BALANCE" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE( INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into AVIA.CUSTOMER
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index CUSTOMER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."CUSTOMER_PK" ON "AVIA"."CUSTOMER" ("CUS_CODE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table CUSTOMER
--------------------------------------------------------

  ALTER TABLE "AVIA"."CUSTOMER" MODIFY ("CUS_CODE" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."CUSTOMER" ADD CONSTRAINT "CUSTOMER_PK" PRIMARY KEY ("CUS_CODE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "USERS"  ENABLE;

Insert into AVIA.Customer
(CUS_CODE, 
CUS_LNAME, 
	CUS_FNAME, 
	CUS_INITIAL, 
	CUS_AREACODE, 
	CUS_PHONE, 
	CUS_BALANCE
   )
VALUES 
( 1,
'Huang',
'Chuck',
'CH',
'312',
'312-477-6199',
1000
);

Insert into AVIA.Customer
(CUS_CODE, 
CUS_LNAME, 
	CUS_FNAME, 
	CUS_INITIAL, 
	CUS_AREACODE, 
	CUS_PHONE, 
	CUS_BALANCE
   )
VALUES 
( 2,
'Li',
'Brian',
'BL',
'222',
'222-477-222',
2000
);

Insert into AVIA.Customer
(CUS_CODE, 
CUS_LNAME, 
	CUS_FNAME, 
	CUS_INITIAL, 
	CUS_AREACODE, 
	CUS_PHONE, 
	CUS_BALANCE
   )
VALUES 
( 3,
'Chu',
'Chealsea',
'',
'333',
'333-477-333',
200
);

Insert into AVIA.Customer
(CUS_CODE, 
CUS_LNAME, 
	CUS_FNAME, 
	CUS_INITIAL, 
	CUS_AREACODE, 
	CUS_PHONE, 
	CUS_BALANCE
   )
VALUES 
( 4,
'Oley',
'David',
'DD',
'333',
'333-477-333',
4000
);