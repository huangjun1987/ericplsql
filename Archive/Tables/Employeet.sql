--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table EMPLOYEE
--------------------------------------------------------

  CREATE TABLE "AVIA"."EMPLOYEE" 
   (	"EMP_NUM" NUMBER, 
	"EMP_TITLE" VARCHAR2(20 BYTE), 
	"EMP_LNAME" VARCHAR2(20 BYTE), 
	"EMP_FNAME" VARCHAR2(20 BYTE), 
	"EMP_INITIAL" VARCHAR2(20 BYTE), 
	"EMP_DOB" DATE, 
	"EMP_HIRE_DATE" DATE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AVIA.EMPLOYEE
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index EMPLOYEE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."EMPLOYEE_PK" ON "AVIA"."EMPLOYEE" ("EMP_NUM") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table EMPLOYEE
--------------------------------------------------------

  ALTER TABLE "AVIA"."EMPLOYEE" MODIFY ("EMP_NUM" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."EMPLOYEE" ADD CONSTRAINT "EMPLOYEE_PK" PRIMARY KEY ("EMP_NUM")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;

insert into AVIA.EMPLOYEE
  VALUES
  (1,
  'Manager',
  'Aden',
  'Ava',
  'AA',
  TO_DATE('1981/01/01 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
TO_DATE('2010/01/01 21:02:44', 'yyyy/mm/dd hh24:mi:ss')
  )