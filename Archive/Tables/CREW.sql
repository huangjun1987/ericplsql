--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CREW
--------------------------------------------------------

  CREATE TABLE "AVIA"."CREW" 
   (	"CHAR_TRIP" NUMBER, 
	"EMP_NUM" NUMBER, 
	"CREW_JOB" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AVIA.CREW
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index CREW_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."CREW_PK" ON "AVIA"."CREW" ("CHAR_TRIP", "EMP_NUM") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table CREW
--------------------------------------------------------

  ALTER TABLE "AVIA"."CREW" MODIFY ("CHAR_TRIP" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."CREW" MODIFY ("EMP_NUM" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."CREW" ADD CONSTRAINT "CREW_PK" PRIMARY KEY ("CHAR_TRIP", "EMP_NUM")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CREW
--------------------------------------------------------

  ALTER TABLE "AVIA"."CREW" ADD CONSTRAINT "CREW_FK1" FOREIGN KEY ("CHAR_TRIP")
	  REFERENCES "AVIA"."CHARTER" ("CHAR_TRIP") ENABLE;
  ALTER TABLE "AVIA"."CREW" ADD CONSTRAINT "CREW_FK2" FOREIGN KEY ("EMP_NUM")
	  REFERENCES "AVIA"."EMPLOYEE" ("EMP_NUM") ENABLE;
      
insert into AVIA.CREW
  VALUES
  (1,
  1,
  'Pilot'
  )
  
  insert into AVIA.CREW
  VALUES
  (2,
  1,
  'Engineer'
  )
  
  insert into AVIA.CREW
  VALUES
  (3,
  1,
  'Engineer'
  )
  
  insert into AVIA.CREW
  VALUES
  (4,
  1,
  'Engineer'
  )