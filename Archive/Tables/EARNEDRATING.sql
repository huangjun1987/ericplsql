--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table EARNEDRATING
--------------------------------------------------------

  CREATE TABLE "AVIA"."EARNEDRATING" 
   (	"EMP_NUM" NUMBER, 
	"RTG_CODE" NUMBER, 
	"EARNRTG_DATE" DATE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AVIA.EARNEDRATING
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index EARNEDRATING_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."EARNEDRATING_PK" ON "AVIA"."EARNEDRATING" ("EMP_NUM", "RTG_CODE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table EARNEDRATING
--------------------------------------------------------

  ALTER TABLE "AVIA"."EARNEDRATING" MODIFY ("EMP_NUM" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."EARNEDRATING" MODIFY ("RTG_CODE" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."EARNEDRATING" ADD CONSTRAINT "EARNEDRATING_PK" PRIMARY KEY ("EMP_NUM", "RTG_CODE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EARNEDRATING
--------------------------------------------------------

  ALTER TABLE "AVIA"."EARNEDRATING" ADD CONSTRAINT "EARNEDRATING_FK1" FOREIGN KEY ("EMP_NUM")
	  REFERENCES "AVIA"."EMPLOYEE" ("EMP_NUM") ENABLE;
  ALTER TABLE "AVIA"."EARNEDRATING" ADD CONSTRAINT "EARNEDRATING_FK2" FOREIGN KEY ("RTG_CODE")
	  REFERENCES "AVIA"."RATING" ("RTG_CODE") ENABLE;
