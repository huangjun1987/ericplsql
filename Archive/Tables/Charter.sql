--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CHARTER
--------------------------------------------------------

  CREATE TABLE "AVIA"."CHARTER" 
   (	"CHAR_TRIP" NUMBER, 
	"CHAR_DATE" DATE, 
	"AC_NUMBER" NUMBER, 
	"CHAR_DESTINATION" VARCHAR2(20 BYTE), 
	"CHAR_DISTANCE" NUMBER, 
	"CHAR_HOURS_FLOWN" NUMBER, 
	"CHAR_HOURS_WAIT" NUMBER, 
	"CHAR_FUEL_GALLONS" NUMBER, 
	"CHAR_OIL_QTS" NUMBER, 
	"CUS_CODE" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AVIA.CHARTER
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index CHARTER_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."CHARTER_PK" ON "AVIA"."CHARTER" ("CHAR_TRIP") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table CHARTER
--------------------------------------------------------

  ALTER TABLE "AVIA"."CHARTER" MODIFY ("CHAR_TRIP" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."CHARTER" ADD CONSTRAINT "CHARTER_PK" PRIMARY KEY ("CHAR_TRIP")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CHARTER
--------------------------------------------------------

  ALTER TABLE "AVIA"."CHARTER" ADD CONSTRAINT "CHARTER_FK1" FOREIGN KEY ("AC_NUMBER")
	  REFERENCES "AVIA"."AIRCRAFT" ("AC_NUMBER") ENABLE;
  ALTER TABLE "AVIA"."CHARTER" ADD CONSTRAINT "CHARTER_FK2" FOREIGN KEY ("CUS_CODE")
	  REFERENCES "AVIA"."CUSTOMER" ("CUS_CODE") ENABLE;
      
insert into AVIA.CHARTER
VALUES
(1,
TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
1,
'Chicago',
1000,
10,
0.2,
100,
11,
1
)

insert into AVIA.CHARTER
VALUES
(2,
TO_DATE('2013/05/02 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
2,
'Seattle',
200,
10,
2,
100,
11,
1
)

insert into AVIA.CHARTER
VALUES
(3,
TO_DATE('2019/05/02 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
2,
'Seattle',
200,
10,
2,
100,
11,
1
)

insert into AVIA.CHARTER
VALUES
(4,
TO_DATE('2020/02/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
1,
'Chicago',
2121,
10,
0.5,
100,
11,
1
)

insert into AVIA.CHARTER
VALUES
(5,
TO_DATE('2020/02/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
1,
'Chicago',
2121,
10,
2,
100,
11,
2
)

insert into AVIA.CHARTER
VALUES
(6,
TO_DATE('2020/02/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),
1,
'Chicago',
2121,
10,
2,
100,
11,
2
)
