--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table MODEL
--------------------------------------------------------

  CREATE TABLE "AVIA"."MODEL" 
   (	"MOD_CODE" NUMBER(*,0), 
	"MOD_MANUFACTURER" VARCHAR2(20 BYTE), 
	"MOD_NAME" VARCHAR2(20 BYTE), 
	"MOD_SEATS" VARCHAR2(20 BYTE), 
	"MOD_CHG_MILE" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AVIA.MODEL
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index MODEL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."MODEL_PK" ON "AVIA"."MODEL" ("MOD_CODE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table MODEL
--------------------------------------------------------

  ALTER TABLE "AVIA"."MODEL" MODIFY ("MOD_CODE" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."MODEL" ADD CONSTRAINT "MODEL_PK" PRIMARY KEY ("MOD_CODE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
  
  insert into AVIA.MODEL
  VALUES
  (1,
  'MOD_MANUFACTURER1',
  'MOD_NAME1',
  '20',
  100
  )
insert into AVIA.MODEL
  VALUES
  (2,
  'MOD_MANUFACTURER2',
  'MOD_NAME2',
  '20',
  2
  )