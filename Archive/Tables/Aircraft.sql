--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table AIRCRAFT
--------------------------------------------------------

  CREATE TABLE "AVIA"."AIRCRAFT" 
   (	"AC_NUMBER" NUMBER(*,0), 
	"MOD_CODE" NUMBER, 
	"AC_TTAF" NUMBER(*,0), 
	"AC_TTEL" NUMBER(*,0), 
	"AC_TTER" NUMBER(*,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AVIA.AIRCRAFT
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index AIRCRAFT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."AIRCRAFT_PK" ON "AVIA"."AIRCRAFT" ("AC_NUMBER") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table AIRCRAFT
--------------------------------------------------------

  ALTER TABLE "AVIA"."AIRCRAFT" MODIFY ("AC_NUMBER" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."AIRCRAFT" ADD CONSTRAINT "AIRCRAFT_PK" PRIMARY KEY ("AC_NUMBER")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table AIRCRAFT
--------------------------------------------------------

  ALTER TABLE "AVIA"."AIRCRAFT" ADD CONSTRAINT "AIRCRAFT_FK1" FOREIGN KEY ("MOD_CODE")
	  REFERENCES "AVIA"."MODEL" ("MOD_CODE") ENABLE;

 insert into AVIA.AIRCRAFT
  VALUES
  (1,
  1,
  1,
  2,
  3
  )
  
  insert into AVIA.AIRCRAFT
  VALUES
  (2,
  2,
  2,
  2,
  3
  )
