--------------------------------------------------------
--  File created - Thursday-March-19-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table REFERRAL
--------------------------------------------------------

  CREATE TABLE "AVIA"."REFERRAL" 
   (	"CUS_CODE" NUMBER, 
	"REF_CODE" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into AVIA.REFERRAL
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index REFERRAL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "AVIA"."REFERRAL_PK" ON "AVIA"."REFERRAL" ("CUS_CODE", "REF_CODE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table REFERRAL
--------------------------------------------------------

  ALTER TABLE "AVIA"."REFERRAL" MODIFY ("CUS_CODE" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."REFERRAL" MODIFY ("REF_CODE" NOT NULL ENABLE);
  ALTER TABLE "AVIA"."REFERRAL" ADD CONSTRAINT "REFERRAL_PK" PRIMARY KEY ("CUS_CODE", "REF_CODE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REFERRAL
--------------------------------------------------------

  ALTER TABLE "AVIA"."REFERRAL" ADD CONSTRAINT "REFERRAL_FK1" FOREIGN KEY ("CUS_CODE")
	  REFERENCES "AVIA"."CUSTOMER" ("CUS_CODE") ENABLE;
  ALTER TABLE "AVIA"."REFERRAL" ADD CONSTRAINT "REFERRAL_FK2" FOREIGN KEY ("REF_CODE")
	  REFERENCES "AVIA"."CUSTOMER" ("CUS_CODE") ENABLE;
