/*
Question-1
For each member and author display the following information.
? Type � Which denotes if it�s a member or an author
? ID � Display the author ID or member ID
? First Name � Display the first name of the member or author
? Last Name � Display the last name of the member or author
*/
-- Step 1: create a union query without Book Count
SELECT 'Author' as Type, a.Au_ID as ID, a.Au_FName as "First Name", a.Au_LName as "Last Name"
FROM LMS.AUTHORS a
UNION
SELECT 'Member' as Type, m.Member_ID as ID, m.First_Name as "First Name", Last_Name as "Last Name"
FROM LMS.MEMBERS m

-- Step 2: Get Book Count of 1 author
select count(*) from LMS.BOOKS_AUTHORS ba where ba.AU_ID = 1
-- Step 3: include this subquery into parent
SELECT 'Author' as Type, a.Au_ID as ID, a.Au_FName as "First Name", a.Au_LName as "Last Name",
(select count(*) from LMS.BOOKS_AUTHORS ba where ba.AU_ID = a.AU_ID) As "Book Count"
FROM LMS.AUTHORS a
-- Step 4: Get Book Count of 1 member
select count(distinct(c.Lib_Item_ID)) 
from LMS.ISSUE_DETAILS d join LMS.LIBRARY_ITEM_COPIES c on c.Item_Copy_ID = d.Item_Copy_ID
where d.Member_ID = 1
-- Step 5: include this subquery into parent
SELECT 'Member' as Type, Member_ID as ID, First_Name as "First Name", Last_Name as "Last Name",
(
select count(distinct(c.Lib_Item_ID)) 
from LMS.ISSUE_DETAILS d join LMS.LMS.LIBRARY_ITEM_COPIES c on c.Item_Copy_ID = d.Item_Copy_ID
where d.Member_ID = m.Member_ID
) As "Book Count"
FROM LMS.MEMBERS m
/*
? Book Count � Display the total number of books written by authors. For members, display the number of books issued to the member (if the same book 
 /*
 Question-6
a) This question requires you to determine, for the past 12 months (exclude the current month) the corresponding data about checkouts. 
See sample below for column headings and data formatting (e.g., for theis issued repeatedly it should count only once). Also display 0 as the count for those authors who have not yet written a book or members who have not been issued a book.
Sort the results by the last name of the member or author.
Your output should like the following (including column headings): Type ID First Name Last Name Book Count
Hint: Review lecture + SQL tutorial on the use of Union. Tutorial has examples on how to display literals.
*/
SELECT 'Author' as Type, a.Au_ID as ID, a.Au_FName as "First Name", a.Au_LName as "Last Name",
(select count(*) from LMS.BOOKS_AUTHORS ba where ba.AU_ID = a.AU_ID) As "Book Count"
FROM LMS.AUTHORS a
UNION
SELECT 'Member' as Type, Member_ID as ID, First_Name as "First Name", Last_Name as "Last Name",
(
select count(distinct(c.Lib_Item_ID)) 
from LMS.ISSUE_DETAILS d join LMS.LIBRARY_ITEM_COPIES c on c.Item_Copy_ID = d.Item_Copy_ID
where d.Member_ID = m.Member_ID
) As "Book Count"
FROM LMS.MEMBERS m
order by "Last Name"

/*
Question-2
a) For each member, display his/her member ID (heading: Member ID), member first name and member last name combined into one column (heading: Member Name), member_since date (heading: Member Since). 
Only show members who joined during or after 2016.
*/
SELECT m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
m.Member_Since As "Member Since"
FROM LMS.MEMBERS m
where m.Member_Since >= TO_DATE('2016/01/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss')
/*
b) Find all the library items ever issued to that member. Display title of the item (heading: Item Title). 
Note: the title itself may not be unique, but you should display separate output rows if it�s a different item. 
If the member hasn�t been issued any items, show N/A under the Item Title column.
*/
--Step 1: Get Member id, Item Id, Title
select  d.Member_ID, i.Lib_Item_ID, i.Title
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title

--Step 2: Include subquery into parent
SELECT m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
m.Member_Since As "Meamber Since",
COALESCE(ic.Title, 'N/A') as "Item Title"
FROM LMS.MEMBERS m 
Left OUTER JOIN  
(
select  d.Member_ID, i.Lib_Item_ID, i.Title
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title
) ic on ic.Member_ID = m.Member_ID
where m.Member_Since >= TO_DATE('2016/01/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss')

/*
c) For each library item (i.e., item, not copy), show how many times the member has been issued that item (heading: Item Issued). 
*/
--Step 1: Get Item Issued
select  d.Member_ID, i.Lib_Item_ID, i.Title, count(d.Issue_Detail_ID) as ItemIssued
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title
--Step 2: Include subquery into parent
WITH LMS.tempTableMemberBorrowHistory(Member_ID, Lib_Item_ID, Title, ItemIssued) as 
(
select  d.Member_ID, i.Lib_Item_ID, i.Title, count(d.Issue_Detail_ID) as ItemIssued
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title
)
SELECT m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
m.Member_Since As "Meamber Since",
COALESCE(ic.Title, 'N/A') as "Item Title",
COALESCE(ic.ItemIssued, 0) as "Item Issued"
FROM LMS.MEMBERS m 
Left OUTER JOIN  
LMS.tempTableMemberBorrowHistory ic on ic.Member_ID = m.Member_ID
where m.Member_Since >= TO_DATE('2016/01/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss')
/*
E.g. if member Wilbur has been issued X-Men four times (across all copies of that item) and Arizona Sport once, you will show two separate output rows for Wilbur: one row showing he has been issued X-Men four times, and another row showing he has been issued Arizona Sport once.
*/

/*
d) Also display how many checkouts for the member across all items. In other words, not just restricted to the item in the current display row. Use a heading of: Num Checkouts. 
If the member hasn�t been issued any items, show a 0.
*/
-- Step 1: Get checkouts Count of 1 member
select count(*) from LMS.ISSUE_DETAILS where Member_ID = 1
--Step 2: Include subquery into parent
WITH tempTableMemberBorrowHistory(Member_ID, Lib_Item_ID, Title, ItemIssued) as 
(
select  d.Member_ID, i.Lib_Item_ID, i.Title, count(d.Issue_Detail_ID) as ItemIssued
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title
)
SELECT m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
m.Member_Since As "Meamber Since",
COALESCE(ic.Title, 'N/A') as "Item Title",
COALESCE(ic.ItemIssued, 0) as "Item Issued",
(
select count(*) from LMS.ISSUE_DETAILS where Member_ID = m.Member_ID
) as "Num Checkouts"
FROM LMS.MEMBERS m 
Left OUTER JOIN  
tempTableMemberBorrowHistory ic on ic.Member_ID = m.Member_ID
where m.Member_Since >= TO_DATE('2016/01/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss')

/*
e) Show the ratio of: Item Issued / Num Checkouts (i.e., ratio of the numbers calculated in part d) and e) ). Heading: Checkout Ratio, format: 0.99.
*/
WITH tempTableMemberBorrowHistory(Member_ID, Lib_Item_ID, Title, ItemIssued) as 
(
select  d.Member_ID, i.Lib_Item_ID, i.Title, count(d.Issue_Detail_ID) as ItemIssued
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title
)
select "Member ID",
"Member Name",
"Member Since",
"Item Title",
"Item Issued",
"Num Checkouts",
to_char(
CASE 
    WHEN "Num Checkouts" = 0 THEN 0
    ELSE "Item Issued"/"Num Checkouts"
END, '0.99')
 AS "Checkout Ratio"
from 
(
SELECT m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
m.Member_Since As "Meamber Since",
COALESCE(ic.Title, 'N/A') as "Item Title",
COALESCE(ic.ItemIssued, 0) as "Item Issued",
(
select count(*) from LMS.ISSUE_DETAILS where Member_ID = m.Member_ID
) as "Num Checkouts"
FROM LMS.MEMBERS m 
Left OUTER JOIN  
tempTableMemberBorrowHistory ic on ic.Member_ID = m.Member_ID
where m.Member_Since >= TO_DATE('2016/01/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss')
)


/*
f) Sort the results by member id and item title.
Hint: Review OUTER JOIN, WITH clause / inline view.
*/
WITH tempTableMemberBorrowHistory(Member_ID, Lib_Item_ID, Title, ItemIssued) as 
(
select  d.Member_ID, i.Lib_Item_ID, i.Title, count(d.Issue_Detail_ID) as ItemIssued
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title
)
select "Member ID",
"Member Name",
"Meamber Since",
"Item Title",
"Item Issued",
"Num Checkouts",
to_char(
CASE 
    WHEN "Num Checkouts" = 0 THEN 0
    ELSE "Item Issued"/"Num Checkouts"
END, '0.99')
 AS "Checkout Ratio"
from 
(
SELECT m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
m.Member_Since As "Meamber Since",
COALESCE(ic.Title, 'N/A') as "Item Title",
COALESCE(ic.ItemIssued, 0) as "Item Issued",
(
select count(*) from LMS.ISSUE_DETAILS where Member_ID = m.Member_ID
) as "Num Checkouts"
FROM LMS.MEMBERS m 
Left OUTER JOIN  
tempTableMemberBorrowHistory ic on ic.Member_ID = m.Member_ID
where m.Member_Since >= TO_DATE('2016/01/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss')
)
order by "Member ID", "Item Title"

/*
Question-3
For each author, display the author ID (Heading: Author ID), and author full name (Author Name, a single column with the last name followed by the first name with a comma in between; 
see the sample below). 
*/
select a.Au_ID as "Author ID", a.Au_LName||', '||a.Au_FName as "Author Name"
from LMS.authors a
/*
We also want a cross-tabulation of the number of books written for each genre by that author (including the total).
*/
--Step 1: query author books
select a.Au_ID as "Author ID", a.Au_LName||', '||a.Au_FName as "Author Name", b.genre,
   (select count(*) from LMS.BOOKS_AUTHORS where Au_ID = a.Au_ID) as "Total Books"
from LMS.authors a 
join LMS.BOOKS_AUTHORS ba on ba.Au_ID = a.Au_ID
join LMS.books b on b.Book_ID = ba.Book_ID
--Step 2: Pivot on Genre
With authorBookGenre("Author ID","Author Name",genre, "Total Books") as
(select a.Au_ID as "Author ID", a.Au_LName||', '||a.Au_FName as "Author Name", b.genre,
   (select count(*) from LMS.BOOKS_AUTHORS where Au_ID = a.Au_ID) as "Total Books"
from LMS.authors a 
join LMS.BOOKS_AUTHORS ba on ba.Au_ID = a.Au_ID
join LMS.books b on b.Book_ID = ba.Book_ID
)
select "Author ID", "Author Name", 
"Technology Books","Mystery Books", 
"Fantasy Books","Science Fiction Books",
"Science Fiction Books","Nature Books",
"Total Books"
from authorBookGenre
pivot 
(
   count(genre)
   for genre in ('Technology Books' as "Technology Books",'Mystery Books' as "Mystery Books",'Fantasy Books' as "Fantasy Books",
   'Science Fiction Books' as "Science Fiction Books",'Nature Books' as "Nature Books")
)

select distinct(
/*
See the sample output seen below, including for headings and number formatting (note: you don�t need to wrap the headings, that�s only done below because of the page width limitation).
Notes:
? Do not display authors that have written NO books.
? Only show authors that have written fantasy books but no technology books.
? Sort the results by total books (highest first), and within that author name (alphabetically).
*/
With authorBookGenre("Author ID","Author Name",genre, "Total Books") as
(select a.Au_ID as "Author ID", a.Au_LName||', '||a.Au_FName as "Author Name", b.genre,
   (select count(*) from LMS.BOOKS_AUTHORS where Au_ID = a.Au_ID) as "Total Books"
from LMS.authors a 
join LMS.BOOKS_AUTHORS ba on ba.Au_ID = a.Au_ID
join LMS.books b on b.Book_ID = ba.Book_ID
)
select "Author ID", "Author Name", 
"Technology Books","Mystery Books", 
"Fantasy Books","Science Fiction Books",
"Science Fiction Books","Nature Books",
"Total Books"
from authorBookGenre
pivot 
(
   count(genre)
   for genre in ('Technology Books' as "Technology Books",'Mystery Books' as "Mystery Books",'Fantasy Books' as "Fantasy Books",
   'Science Fiction Books' as "Science Fiction Books",'Nature Books' as "Nature Books")
)
where "Total Books" > 0 and "Technology Books" = 0 and "Fantasy Books" > 0
order by "Total Books" desc, "Author Name" 

/*This query first uses the WITH keyword to create an inline view of AuthorBookGenre, then uses the PIVOT keyword to turn each genre's count into its own column.*/

/*
Question-4
a) For every member with a non-negative Member Balance, display:
? Member ID (heading: Member ID)
? Member first name and member last name combined into one column with a space in between (e.g., Wilma Wildcat; heading: Member Name)
? The years they have been a member (or were a member, if their membership expired), rounded up to the nearest whole number (heading: Mem Years). E.g., if they joined in August 2019, show 1 instead of 0.
? Member balance with a heading of: Member Balance (USD) Format the member balance using a $9,999.99 template.
*/
select m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
CEIL(MONTHS_BETWEEN(CURRENT_DATE , m.Member_Since)/12) As "Mem Years"
FROM LMS.MEMBERS m 
where m.Balance >=0
/*
b) Display the number of checkouts for each member. Include items issued more than once. 
E.g., if Wilbur Wildcat has borrowed the same item, i.e., �Database Rules� twice, and another item, �I love Data� thrice, this column should show 5. 
(heading: Num Checkouts). Show 0 if they have borrowed no items.
*/
select m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
CEIL(MONTHS_BETWEEN(CURRENT_DATE , m.Member_Since)/12) As "Mem Years",
to_char(m.Balance, '$9,999.99') as "Member Balance",
(
select count(*) from LMS.ISSUE_DETAILS where Member_ID = m.Member_ID
) as "Num Checkouts"
FROM LMS.MEMBERS m 
where m.Balance >=0
/*
c) Display the number of times members have borrowed items in the current year (show 0 if they have borrowed none). 
Instead of hard coding the year, please dynamically determine the year using SYSDATE. Column heading: Num Checkouts YTD
*/
select m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
CEIL(MONTHS_BETWEEN(CURRENT_DATE , m.Member_Since)/12) As "Mem Years",
to_char(m.Balance, '$9,999.99') as "Member Balance",
(
select count(*) from LMS.ISSUE_DETAILS where Member_ID = m.Member_ID
) as "Num Checkouts",
(
select count(*) from LMS.ISSUE_DETAILS where Member_ID = m.Member_ID and trunc(issue_date, 'YEAR') = trunc(sysdate, 'YEAR')
) as "Num Checkouts YTD"
FROM LMS.MEMBERS m 
where m.Balance >=0
/*
d) Display the total number of pages potentially read by each member based on each library item that has been issued to them (heading: Total Pages Read). 
In this case, do not double count the pages if an item was issued to a member more than once. 
E.g., if Wilbur Wildcat borrows �Database Rules� twice, and the item has 1200 pages, your query should calculate this as 1200 pages not 2400. Format the total pages column as: 999,999
Hint: Review the WITH clause. Also review SQL tutorial to see how to round up.
*/
--Step 1:Query for MemberBorrowHistory
select  d.Member_ID, i.Lib_Item_ID, i.Title, i.No_Of_Pages
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title, i.No_Of_Pages
--Step 2: Use WITH
WITH MemberBorrowHistory(Member_ID, Lib_Item_ID, Title, No_Of_Pages) as 
(
select  d.Member_ID, i.Lib_Item_ID, i.Title, i.No_Of_Pages
from
LMS.ISSUE_DETAILS d join 
LMS.LIBRARY_ITEM_COPIES c on d.Item_Copy_ID = c.Item_Copy_ID
JOIN  LMS.LIBRARY_ITEMS i on i.Lib_Item_ID = c.Lib_Item_ID
group by  d.Member_ID, i.Lib_Item_ID, i.Title, i.No_Of_Pages
)
select m.Member_ID as "Member ID", m.First_Name || ' '||m.Last_Name as "Member Name",
CEIL(MONTHS_BETWEEN(CURRENT_DATE , m.Member_Since)/12) As "Mem Years",
to_char(m.Balance, '$9,999.99') as "Member Balance",
(
select count(*) from LMS.ISSUE_DETAILS where Member_ID = m.Member_ID
) as "Num Checkouts",
(
select count(*) from LMS.ISSUE_DETAILS where Member_ID = m.Member_ID and trunc(issue_date, 'YEAR') = trunc(sysdate, 'YEAR')
) as "Num Checkouts YTD",
COALESCE(
(
SELECT SUM(mbh.No_Of_Pages) FROM MemberBorrowHistory mbh WHERE mbh.member_id = m.member_id
)
, 0)
 AS "Total Pages Read"
FROM LMS.MEMBERS m 
where m.Balance >=0

/*This query uses MONTHS_BETWEEN to calculate member years and uses WITH to create an inline view for MemberBorrowHistory then uses count and sum to calcuate the statistics */

/*
Question-5
a) For each publisher, show their name (heading: Publisher Name), the number of items the library owns (that are published) by that publisher (heading: Num Items), 
the number of library item copies the library owns (of items published) by that publisher (heading: Num Library Copies). 
Sort the output by the Publisher Name. Note: theoretically, the publisher name may not be always unique. 
So, consider that while writing your query to ensure is always logically correct. 
Show a 0 if LMS does not currently own any items (or copies of an item) by a publisher.
*/
select p.pub_name as "Publisher Name",
(
select count(Lib_item_Id) from LMS.library_items where Pub_ID = p.Pub_ID 
) as "Num Items",
(
select count(Item_Copy_ID) from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
) as "Num Library Copies"
 from LMS.Publishers p
 order by p.pub_name
/*
b) Display the total number of times library copies by that publisher have been issued (heading: Total Checkouts)
*/
select p.pub_name as "Publisher Name",
(
select count(Lib_item_Id) from LMS.library_items where Pub_ID = p.Pub_ID 
) as "Num Items",
(
select count(Item_Copy_ID) from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
) as "Num Library Copies",
(
select count(Issue_Detail_ID) from LMS.issue_details where Item_Copy_ID in (
select Item_Copy_ID from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
)
) as "Total Checkouts"
 from LMS.Publishers p
 order by p.pub_name
/*
c) Also show the total purchase amount spent on all of the library item copies from that publisher (heading: Total Purchase Cost).
Format as $99,999.99 and display as 0 if no items have been purchased from that publisher.
*/
select p.pub_name as "Publisher Name",
(
select count(Lib_item_Id) from LMS.library_items where Pub_ID = p.Pub_ID 
) as "Num Items",
(
select count(Item_Copy_ID) from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
) as "Num Library Copies",
(
select count(Issue_Detail_ID) from LMS.issue_details where Item_Copy_ID in (
select Item_Copy_ID from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
)
) as "Total Checkouts",
to_char(
COALESCE(
(
select SUM(Purchase_Price) from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
)
, 0)
,'$99,999.99') as "Total Purchase Cost"
 from LMS.Publishers p
 order by p.pub_name
/*
d) Display the publisher categorization (Heading: Publisher Type) based on the number of checkouts its copies have.
a. Gold Publisher: the total checkouts > 25
b. Silver Publisher: the total checkouts > 15 (but not enough to be Gold)
c. Bronze Publisher: the total checkouts > 0 (but not enough to be Silver)
d. New Publisher: the library has not issued out any library item copies by this publisher
Hint: Review functions: CASE(), RANK() [including the PARTITION BY + ORDER BY clauses for RANK()].
*/
WITH PublisherStats("Publisher Name", "Num Items", "Num Library Copies", "Total Checkouts", "Total Purchase Cost") AS (
select p.pub_name as "Publisher Name",
(
select count(Lib_item_Id) from LMS.library_items where Pub_ID = p.Pub_ID 
) as "Num Items",
(
select count(Item_Copy_ID) from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
) as "Num Library Copies",
(
select count(Issue_Detail_ID) from LMS.issue_details where Item_Copy_ID in (
select Item_Copy_ID from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
)
) as "Total Checkouts",
to_char(
COALESCE(
(
select SUM(Purchase_Price) from LMS.library_item_copies where Lib_Item_ID in (
select Lib_item_Id from LMS.library_items where Pub_ID = p.Pub_ID 
) 
)
, 0)
,'$99,999.99') as "Total Purchase Cost"
 from LMS.Publishers p
 order by p.pub_name
)
select "Publisher Name", "Num Items", "Num Library Copies", "Total Checkouts", "Total Purchase Cost",
(CASE 
    WHEN "Total Checkouts" > 25 THEN 'Gold Publisher'
    WHEN "Total Checkouts" > 15 and "Total Checkouts" <= 25 THEN 'Silver Publisher'
    WHEN "Total Checkouts" > 0 and "Total Checkouts" <= 15 THEN 'Bronze Publisher'
    ELSE 'New Publisher'
END) as "Publisher Type"
 from PublisherStats

/*This query uses WITH to create an inline view of Publisher Stats, then uses CASE to assign appropriate Publisher Type */ 
 year and month, you should have separate columns with the year and month showing). 
You can assume we have checkout (library issue details) data for each month.
? The �current� year and month, e.g., (2019, 10) or (2019, 9), etc.
? The number of checkouts in that month
? The number of checkouts from a month ago (e.g., if we�re in October 2019, you should get September 2019 checkouts). If we have no data for a month ago, show: N/A
? The monthly change in checkouts (i.e., this month�s checkouts � checkouts from a month ago). If there is no data, show N/A.
? The number of checkouts in the following month (if we have data, i.e., it�s not in the future).
b) Sort the results by month so the latest month is on top (e.g., in the sample below, data for October, the 10th month, is before data for September, the 9th month).
Please ensure your query is dynamic (i.e., don�t �hard code� values, because otherwise the query will not work in future months and years).
*/
--Step 1: Query monthly checkout
select EXTRACT(year FROM issue_date), EXTRACT(month FROM issue_date), count(ISSUE_DETAIL_ID)  from LMS.ISSUE_DETAILS
group by EXTRACT(year FROM issue_date), EXTRACT(month FROM issue_date)
order by EXTRACT(year FROM issue_date), EXTRACT(month FROM issue_date)

--Step 2
With MonthlyCheckout(Year, Month, Checkouts) as (
select EXTRACT(year FROM issue_date), EXTRACT(month FROM issue_date), count(ISSUE_DETAIL_ID)  from LMS.ISSUE_DETAILS
where issue_date between trunc(add_months(sysdate, -12), 'MONTH') and TRUNC(sysdate, 'MONTH')
group by EXTRACT(year FROM issue_date), EXTRACT(month FROM issue_date)
)
select Year, Month, Checkouts,
LEAD(Checkouts, 1, null) OVER (ORDER BY Checkouts) AS "Last Month's Checkouts",
Checkouts - LEAD(Checkouts, 1, 0) OVER (ORDER BY Checkouts) AS "Monthly Change",
LAG(Checkouts, 1, null) OVER (ORDER BY Checkouts) AS "Next Month�s Checkouts"
from MonthlyCheckout
order by Year desc, Month desc






