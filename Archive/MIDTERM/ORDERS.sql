--------------------------------------------------------
--  File created - Friday-April-03-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ORDERS
--------------------------------------------------------

  CREATE TABLE "MIDTERM"."ORDERS" 
   (	"ORDNO" VARCHAR2(20 BYTE), 
	"BOOKID" VARCHAR2(20 BYTE), 
	"QTY" NUMBER, 
	"PRICE" NUMBER, 
	"SHIPPER" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into MIDTERM.ORDERS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index ORDERS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MIDTERM"."ORDERS_PK" ON "MIDTERM"."ORDERS" ("ORDNO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table ORDERS
--------------------------------------------------------

  ALTER TABLE "MIDTERM"."ORDERS" MODIFY ("ORDNO" NOT NULL ENABLE);
  ALTER TABLE "MIDTERM"."ORDERS" MODIFY ("BOOKID" NOT NULL ENABLE);
  ALTER TABLE "MIDTERM"."ORDERS" ADD CONSTRAINT "ORDERS_PK" PRIMARY KEY ("ORDNO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ORDERS
--------------------------------------------------------

  ALTER TABLE "MIDTERM"."ORDERS" ADD CONSTRAINT "ORDERS_FK1" FOREIGN KEY ("BOOKID")
	  REFERENCES "MIDTERM"."BOOKS" ("BOOKID") ENABLE;

insert into ORDERS
values 
(
'AX014',
'B001',
250 ,
21.50,
'UPS'
)


insert into ORDERS
values 
(
'BA001',
'B103',
500  ,
19.75,
'DHL'
)