--------------------------------------------------------
--  File created - Friday-April-03-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table BOOKS
--------------------------------------------------------

  CREATE TABLE "MIDTERM"."BOOKS" 
   (	"BOOKID" VARCHAR2(20 BYTE), 
	"AUID" VARCHAR2(20 BYTE), 
	"BTITLE" VARCHAR2(20 BYTE), 
	"TOTALSALES" NUMBER, 
	"BTYPE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into MIDTERM.BOOKS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index BOOKS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MIDTERM"."BOOKS_PK" ON "MIDTERM"."BOOKS" ("BOOKID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table BOOKS
--------------------------------------------------------

  ALTER TABLE "MIDTERM"."BOOKS" MODIFY ("BOOKID" NOT NULL ENABLE);
  ALTER TABLE "MIDTERM"."BOOKS" MODIFY ("AUID" NOT NULL ENABLE);
  ALTER TABLE "MIDTERM"."BOOKS" ADD CONSTRAINT "BOOKS_PK" PRIMARY KEY ("BOOKID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table BOOKS
--------------------------------------------------------

  ALTER TABLE "MIDTERM"."BOOKS" ADD CONSTRAINT "BOOKS_FK1" FOREIGN KEY ("AUID")
	  REFERENCES "MIDTERM"."AUTHORS" ("AUID") ENABLE;

insert into BOOKS
values
('B001',
'A003',
'Joy of Databases',
25000,
'tech'
)


insert into BOOKS
values
('B015',
'A003',
'Fun with Networks',
25000,
'tech'
)

insert into BOOKS
values
('B103',
'A002',
'New Cookbook',
100,
'cooking'
)