--------------------------------------------------------
--  File created - Friday-April-03-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table AUTHORS
--------------------------------------------------------

  CREATE TABLE "MIDTERM"."AUTHORS" 
   (	"AUID" VARCHAR2(20 BYTE), 
	"AUNAME" VARCHAR2(20 BYTE), 
	"CITY" VARCHAR2(20 BYTE), 
	"STATE" VARCHAR2(20 BYTE), 
	"COUNTRY" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
REM INSERTING into MIDTERM.AUTHORS
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index TABLE1_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "MIDTERM"."TABLE1_PK" ON "MIDTERM"."AUTHORS" ("AUID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table AUTHORS
--------------------------------------------------------

  ALTER TABLE "MIDTERM"."AUTHORS" MODIFY ("AUID" NOT NULL ENABLE);
  ALTER TABLE "MIDTERM"."AUTHORS" ADD CONSTRAINT "TABLE1_PK" PRIMARY KEY ("AUID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;
  
  insert into MIDTERM.AUTHORS
  values
  ('A003',
  'Bennett',
  'Tucson',
  'AZ',
  'USA'
  )
  
insert into MIDTERM.AUTHORS
  values
  ('A002',
  'Ringer',
  'Springfield',
  'BC',
  'Canada'
  )
  
insert into MIDTERM.AUTHORS
  values
  ('A003',
  'Bennett',
  'Tucson',
  'AZ',
  'USA'
  )
  
insert into MIDTERM.AUTHORS
  values
  ('A003',
  'Bennett',
  'Tucson',
  'AZ',
  'USA'
  )
