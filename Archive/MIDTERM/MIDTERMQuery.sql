select a.AuID,  a.AuName, (select sum(Qty* Price) from ORDERS
where BookID in (select BookID from BOOKS where AuID = a.AuID)) as Revenue,

from AUTHORS a

WITH AUTHOR_REVENUE(AuID, AuName, REVENUE) as
(select a.AuID,  a.AuName, (select sum(Qty* Price) from ORDERS
where BookID in (select BookID from BOOKS where AuID = a.AuID)) as Revenue from AUTHORS a
)
SELECT 	AUID, AUNAME, REVENUE
FROM 	AUTHOR_REVENUE
where REVENUE = (
select max(REVENUE) from AUTHOR_REVENUE
)

